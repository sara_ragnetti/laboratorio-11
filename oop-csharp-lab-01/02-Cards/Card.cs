﻿namespace Cards
{
    class Card
    {
        public string Seed { get; set; }
        public string Value { get; set; }

        public Card(string value, string seed)
        {
            Seed = seed;
            Value = value;
        }

            
        public override string ToString()
        {
            // TODO comprendere il meccanismo denominato in C# "string interpolation"
            return $"{this.GetType().Name}(Name={Value}, Seed={Seed})";
        }
    }

    
}
